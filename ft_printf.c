/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: basylbek <basylbek@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 15:16:10 by basylbek          #+#    #+#             */
/*   Updated: 2019/10/24 16:27:11 by basylbek         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	handle_flags(const char *format, int *i, va_list argumets)
{
	int		digit;
	char	*s;
	char	c;

	*i = *i + 1;
	if (format[*i] == 'd')
	{
		digit = va_arg(argumets, int);
		ft_putnbr(digit);
	}
	else if (format[*i] == 's')
	{
		s = va_arg(argumets, char*);
		ft_putstr(s);
	}
	else if (format[*i] == 'c')
	{
		c = va_arg(argumets, int);
		ft_putchar(c);
	}
	else if (format[*i] == 'x')
	{
		digit = va_arg(argumets, int);
		ft_putstr(ft_itoa_base(digit, 16));
	}
	else if (format[*i] == 'X')
	{
		digit = va_arg(argumets, int);
		ft_putstr(to_upper_str(ft_itoa_base(digit, 16)));
	}
	else if (format[*i] == 'o')
	{
		digit = va_arg(argumets, int);
		ft_putstr(ft_itoa_base(digit, 8));
	}
	else if (format[*i] == 'p')
	{
		digit = va_arg(argumets, int);
		ft_putnbr(digit);
	}
}

void	ft_printf(const char *format, ...)
{
	int		i;
	va_list arguments;

	va_start(arguments, format);
	i = 0;
	while (format[i] != '\0')
	{
		if (format[i] == '%')
			handle_flags(format, &i, arguments);
		else
			ft_putchar(format[i]);
		i++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putfloat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: basylbek <basylbek@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 16:38:02 by basylbek          #+#    #+#             */
/*   Updated: 2019/10/24 16:42:45 by basylbek         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_itoa_float(double n, int presc)
{
    char *s;
    int i;
    
    s = (char*)malloc(presc + 2);
    i = n;
    ft_strcpy(s, ft_itoa(i));
    return (s); 
}
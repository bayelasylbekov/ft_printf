/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: basylbek <basylbek@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 14:44:26 by basylbek          #+#    #+#             */
/*   Updated: 2019/10/24 23:48:55 by basylbek         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_pow(int n, int base)
{
	int tmp;

	tmp = 1;
	base = (base < 0) ? base * -1 : base;
	while (base--)
		tmp = n * tmp;
	return (tmp);
}


char	*ft_itoa_precs(int n, int pres)
{
	char *s;
	int i;
	int sign;

	sign = 0;
	if (n < 0)
	{
		sign = 1;
		n = n * -1;
	}
	s = ft_itoa(n);
	i = ft_strlen(s);
	while (i++ < pres)
		s = ft_strjoin("0", s);
	if (sign)
		s = ft_strjoin("-", s);
	return (s);
}

char    *ft_itoa_float(float n, int presc, int base)
{
    int		ipart;
	float	fpart;
	char *s;
    
	ipart = (int)n;
	fpart = n - (float)ipart;
	s = ft_itoa_base(ipart, base);
	if (presc != 0)
    {
		s = ft_strjoin(s, ".");
		fpart = fpart * ft_pow(base, presc);
		s = ft_strjoin(s, ft_itoa_precs((int)fpart, presc));
    }
	return (s);
}

int		ft_normalize(float *n)
{
	int i;

	i = 0;
	while ((int)(*n) <= 0)
	{
		*n = *n * 10;
		i--;
	}
	while ((int)(*n) >= 10)
	{
		*n = *n / 10;
		i++;
	}
	return (i);
}


char	*ft_itoa_e(float n, int presc)
{
	char *s;
	char	*mantissa;
	char	*exponent;
	int sign;

	sign = 0;
	if (n < 0.0)
	{
		n = n * -1.0;
		sign = 1;
	}
	exponent = ft_itoa_precs(ft_normalize(&n), 2);
	mantissa = ft_itoa_float(n, presc, 10);
	s = ft_strjoin(mantissa, "e");
	if (exponent[0] != '-')
		s = ft_strjoin(s, "+"); 
	s = ft_strjoin(s, exponent);
	if (sign)
		s = ft_strjoin("-", s);
	return (s);
}

int		main(int ac, char **av)
{
	float f;
	char *s;

	f = 0.003;
	s = ft_itoa_e(f, 6);
	printf("%s\n%e\n", s, f);
	return 0;
}
 
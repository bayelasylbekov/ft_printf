/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: basylbek <basylbek@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 15:47:30 by basylbek          #+#    #+#             */
/*   Updated: 2019/10/23 14:28:44 by basylbek         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef H_FT_PRINTF
# define H_FT_PRINTF
# define ALLSYMBOLS "cspdiouxXfy%#-+ .*0123456789hLljz"

#include <stdarg.h>
#include "libft/libft.h"
#include <stdio.h>

typedef struct		s_struct
{
	char			*format;
	int				nprinted;
	int				i;
	int				len;

	int				minus;
	int				plus;
	int				space;
	int				zero;
	int				hash;
	
	int				width;
	int				precisiontf;
	int				precision;
	int				length;
}					t_struct;
void				ft_printf( const char *format, ... );
# endif
